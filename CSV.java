import javax.swing.*;

public class CSV extends JFrame {
    public CSV() {
        super("List CSV");

        initComponents();

        setSize(500, 480);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initComponents() {
        JPanel panel = new JPanel();
        JTextArea affiche = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(affiche);
        for(int i = 0; i<GraphGestion.getLines().size();i++)
        {
            String text = GraphGestion.getLines().get(i);
            affiche.append(text);
            affiche.append("\n");
        }
        affiche.setEditable(false);
        add(scrollPane);
        add(affiche);
    }
}
