import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Informations extends JPanel implements ActionListener {

    public static void information() {
        JFrame fenInfo = new JFrame();
        JPanel panInfo = new JPanel();

        JLabel lab1 = new JLabel("Informations complémentaires");
        lab1.setFont(new Font("Serif", Font.BOLD, 40));
        lab1.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel lab2 = new JLabel(" Auteur : ");
        lab2.setFont(new Font("Serif",Font.PLAIN, 20));
        lab2.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel lab3 = new JLabel(" Quentin Bastos et Loïc Woss");
        lab3.setFont(new Font("Serif",Font.PLAIN, 20));
        lab3.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel lab4 = new JLabel("ratio");
        lab4.setFont(new Font("Serif",Font.PLAIN, 20));
        lab4.setAlignmentX(Component.CENTER_ALIGNMENT);
        panInfo.setLayout(new BoxLayout(panInfo, BoxLayout.Y_AXIS));
        panInfo.add(lab1);
        panInfo.add(lab2);
        panInfo.add(lab3);
        panInfo.add(lab4);
        fenInfo.setContentPane(panInfo);
        fenInfo.setSize(700, 300);
        fenInfo.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
