import javax.swing.*;
import java.awt.*;

public class AfficherVille extends JFrame {
    public void graph() {
        setSize(1920, 1080);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setContentPane(new AfficherVille.MyPanel());
        setTitle("Affichage du graphe avec uniquement les ville");
        Image icon = Toolkit.getDefaultToolkit().getImage("Iko.png");
        setIconImage(icon);
        setLayout(null);
        setSize(1920, 1080);
        setVisible(true);
    }

    public AfficherVille() {
        super();
        graph();
        setVisible(true);
    }

    static class MyPanel extends JPanel {
        public MyPanel() {
            super();
            setPreferredSize(new Dimension(1300, 1300));
            setBorder(BorderFactory.createLineBorder(Color.black));
            // Ajouter les éléments au menu "affichage"
        }

        public void paint(Graphics p) {
            // Draw Tree Here
            int x = 160;
            double y = 160;
            int xsuiv = 210;
            int ysuiv = 260;
            int j = 0;
            int resto = 0; // nb de resto
            int loisir = 0; // nb de loisir
            int ville = 0; // nb de ville
            int auto = 0; // nb d'autoroute
            int depart = 0; // nb de départementale
            int natio = 0; // nb de nationale

            String test[][];
            test = GraphGestion.getGraphTab();
            System.out.println(test);
            p.setColor(Color.BLUE);
            int nb = GraphGestion.nb_neux(GraphGestion.getLines());
            for (int i = 0; i < nb; ) {
                if (x < 2200) {
                    if (test[i][j].startsWith("V")) {
                        ville++;
                        System.out.println(" ");
                        System.out.print(test[i][j]);
                        p.setColor(Color.BLACK);
                        p.fillOval(x, (int) y, 30, 30);
                        p.setColor(Color.BLACK);
                        p.drawString(test[i][j], x, (int) y);
                        x += PosAleatoire.posx()*2+700;
                        y = PosAleatoire.posy()*2;
                    }
                }
                x = (int) PosAleatoire.posx()*2;
                i++;
            }
            p.setColor(Color.BLACK);
            p.drawString("Villes = " + ville , 40, 20);
            p.fillOval( 10,  15, 15, 15);
            p.setColor(Color.BLACK);
            p.drawRect(0,0,200,50);
        }
    }
}