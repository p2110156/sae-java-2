import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 */
public class AfficherGraph extends JFrame implements ActionListener {

    public AfficherGraph() {
        super();
        graph();
        setVisible(true);
    }

    /**
     * Definition de la fen, taille titre, logo
     */
    public void graph() {
        setSize(1920, 1080);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setContentPane(new MyPanel());
        setTitle("Affichage du graphe");
        Image icon = Toolkit.getDefaultToolkit().getImage("Iko.png");
        setIconImage(icon);
        setLayout(null);
        setSize(100, 100);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    static class MyPanel extends JPanel {
        public MyPanel() {
            super();
            setPreferredSize(new Dimension(300, 300));
            setBorder(BorderFactory.createLineBorder(Color.black));
            // Ajouter les éléments au menu "affichage"
        }


        /**
         * @param g Partie graph de la fen
         */
        @Override
        public void paint(Graphics g) {
            // Draw Tree Here
            String[] words;
            int x = 0;
            int y = 0;
            int xp = 0;
            int yp = 0;
            int j = 0; // variable lié à la matrice
            int i = 0; // variable lié à la matrice
            int z = 0;
            int n = 0;
            int t = 0; // variable pour selectionner les cordonnes d'une ville deja enregistrer
            int resto = 0; // nb de resto
            int loisir = 0; // nb de loisir
            int ville = 0; // nb de ville
            int auto = 0; // nb d'autoroute
            int depart = 0; // nb de départementale
            int natio = 0; // nb de nationale
            String test[][];
            String tab[][] = new String[31][3];
            test = GraphGestion.getGraphTab();
            System.out.println(test);
            g.setColor(Color.BLUE);
            int nb = GraphGestion.nb_neux(GraphGestion.getLines());
            // Appeller plusieurs fois PosAleatoire permet d'activer la fonction aléatoire une première fois et d'éviter les bugs lorsqu'elle s'execute rééelement car (1ere valeur non aléatoire)
            x = (int) PosAleatoire.posx();
            x = (int) PosAleatoire.posx();
            y = (int) PosAleatoire.posy();
            y = (int) PosAleatoire.posy();
            x = (int) PosAleatoire.posx();
            y = (int) PosAleatoire.posy();
            g.setColor(Color.BLACK);
            for (i = 0; i < nb; ) {
                j = 0;
                if (x < 1500) {
                    x += PosAleatoire.posx();
                    y = (int) ((int) PosAleatoire.posy()*1.5);
                } else {
                    x = (int) PosAleatoire.posx();
                    y += PosAleatoire.posy()*1.3;
                }
                if (test[i][0].startsWith("V"))
                {
                    ville++;
                    g.setColor(Color.BLACK);
                    g.fillOval(x, (int) y, 30, 30);
                    g.setColor(Color.BLACK);
                    g.drawString(test[i][j], x, (int) y);
                    tab[i][0] = test[i][0];
                    tab[i][1] = String.valueOf(x);
                    tab[i][2] = String.valueOf(y);
                }
                if (test[i][0].startsWith("L"))
                {
                    loisir++;
                    g.setColor(Color.RED);
                    g.fillOval(x, (int) y, 20, 20);
                    g.setColor(Color.BLACK);
                    g.drawString(test[i][j], x, (int) y);
                    tab[i][0] = test[i][0];
                    tab[i][1] = String.valueOf(x);
                    tab[i][2] = String.valueOf(y);
                }
                if (test[i][0].startsWith("R"))
                {
                    resto++;
                    g.setColor(Color.GREEN);
                    g.fillOval(x, (int) y, 20, 20);
                    g.setColor(Color.BLACK);
                    g.drawString(test[i][j], x, (int) y);
                    tab[i][0] = test[i][0];
                    tab[i][1] = String.valueOf(x);
                    tab[i][2] = String.valueOf(y);
                }
                i++;
            }
            i = 0;
            z = 0;
            j = 1;
            n = 0;
            for (i = 0; i < nb; ) {
                System.out.println("Ville = " + tab[i][0] + "   x ="+  tab[i][1] +"   y ="+ tab[i][2]);
                i++;
            }
            for (i = 0; i < nb; ) {
                while ((!test[z][j].endsWith(("|")))) {
                    words = test[z][j].split("[>]");
                    t = 0;
                    while ((!tab[t][0].contains(words[1]))) {
                        t++;
                    }
                    if (words[1].equals(tab[t][0])) {
                        xp = Integer.parseInt(tab[t][1]);
                        yp = Integer.parseInt(tab[t][2]);
                        x = Integer.parseInt(tab[i][1]);
                        y = Integer.parseInt(tab[i][2]);
                        System.out.println("mot1:"+ tab[i][0]+"   tab0:"+tab[t][0]+"   tab t1 :"   +tab[t][1] + "   tab t2:"+tab[t][2]+"   tab i1:"+tab[i][1]+"   tab i2:"+tab[i][2]); // test programme
                        if (words[0].startsWith("A,"))
                        {
                            g.setColor(Color.BLACK);
                            auto++;
                            g.drawString(words[0], (((x + xp) / 2)-5), (((y + yp) / 2)-5));
                            g.drawLine(x + 10, y + 10, xp + 10, yp + 10);
                            j++;
                        }
                        if (words[0].startsWith("D,"))
                        {
                            g.setColor(Color.RED);
                            depart++;
                            g.drawString(words[0], (((x + xp) / 2)-10), (((y + yp) / 2)-10));
                            g.drawLine(x + 5, y + 5, xp + 5, yp + 5);
                            j++;
                        }
                        if (words[0].startsWith("N,"))
                        {
                            g.setColor(Color.GREEN);
                            natio++;
                            g.drawString(words[0], (x + xp) / 2, (y + yp) / 2);
                            g.drawLine(x + 15, y + 15, xp + 15, yp + 15);
                            j++;
                        }

                    } else {
                        System.out.println("ERROR 404");
                        j++;
                    }
                }
                words = test[z][j].split("[>]");
                t = 0;
                while ((!words[1].contains(tab[t][0]))) {
                    t++;
                }
                if (words[1].contains(tab[t][0])) {
                    xp = Integer.parseInt(tab[t][1]);
                    yp = Integer.parseInt(tab[t][2]);
                    x = Integer.parseInt(tab[i][1]);
                    y = Integer.parseInt(tab[i][2]);
                    System.out.println("mot1:"+ tab[i][0]+"   tab0:"+tab[t][0]+"   tab t1 :"   +tab[t][1] + "   tab t2:"+tab[t][2]+"   tab i1:"+tab[i][1]+"   tab i2:"+tab[i][2]); // test programme
                    if (words[0].startsWith("A,"))
                    {
                        g.setColor(Color.BLACK);
                        auto++;
                        g.drawString(words[0], (((x + xp) / 2)-5), (((y + yp) / 2)-5));
                        g.drawLine(x + 10, y + 10, xp + 10, yp + 10);
                        j++;
                    }
                    if (words[0].startsWith("D,"))
                    {
                        g.setColor(Color.RED);
                        depart++;
                        g.drawString(words[0], (((x + xp) / 2)-10), (((y + yp) / 2)-10));
                        g.drawLine(x + 5, y + 5, xp + 5, yp + 5);
                        j++;
                    }
                    if (words[0].startsWith("N,"))
                    {
                        g.setColor(Color.GREEN);
                        natio++;
                        g.drawString(words[0], (x + xp) / 2, (y + yp) / 2);
                        g.drawLine(x + 15, y + 15, xp + 15, yp + 15);
                        j++;
                    }

                } else {
                    j++;
                    System.out.println("ERROR 404");
                }
                i++;
                z++;
                j = 1;
            }

            g.setColor(Color.BLACK);
            g.drawString("Villes = " + ville, 40, 20);
            g.fillOval(10, 15, 15, 15);
            g.setColor(Color.RED);
            g.drawString("Loisirs = " + loisir, 40, 40);
            g.fillOval(10, 35, 15, 15);
            g.setColor(Color.GREEN);
            g.drawString("Restos = " + resto, 40, 60);
            g.fillOval(10, 55, 15, 15);
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(0, 76, 200, 76);
            g.setColor(Color.BLACK);
            g.drawString("Autoroutes = " + auto/2, 40, 100);
            g.drawLine(10, 90, 30, 100);
            g.setColor(Color.GREEN);
            g.drawString("Nationales = " + natio/2, 40, 120);
            g.drawLine(10, 110, 30, 120);
            g.setColor(Color.RED);
            g.drawString("Départementales = " + depart/2, 40, 140);
            g.drawLine(10, 130, 30, 140);
            g.setColor(Color.BLACK);
            g.drawRect(0, 0, 200, 160);
        }
    }
}