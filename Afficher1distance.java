import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 */
public class Afficher1distance extends JFrame implements ActionListener {

    /**
     * Definition de la fen, taille titre, logo
     */
    public void graph() {
        setSize(1920, 1080);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setContentPane(new MyPanel());
        setTitle("Affichage du graphe");
        Image icon = Toolkit.getDefaultToolkit().getImage("Iko.png");
        setIconImage(icon);
        setLayout(null);
        setSize(100,100);
        setVisible(true);
    }


    public Afficher1distance() {
        super();
        graph();
        setVisible(true);
    }

    static class MyPanel extends JPanel {
        public MyPanel() {
            super();
            setPreferredSize(new Dimension(300,300));
            setBorder(BorderFactory.createLineBorder(Color.black));
            // Ajouter les éléments au menu "affichage"
        }


        /**
         * @param g
         * Partie graph de la fen
         */
        @Override
        public void paint(Graphics g) {
            // Draw Tree Here
            int x = 0;
            int y = 0;
            int xuniq = 0;
            int yuniq = 0;
            int j = 0; // variable lié à la matrice
            int i = 0; // variable lié à la matrice
            int z = 0;
            int resto = 0; // nb de resto
            int loisir = 0; // nb de loisir
            int ville = 0; // nb de ville
            int auto = 0; // nb d'autoroute
            int depart = 0; // nb de départementale
            int natio = 0; // nb de nationale

            String test[][];
            test = GraphGestion.getGraphTab();
            while ((!test[i][j].equals(ChoisirPoint.getPoint1()))) {
                i++;
            }

            if (test[i][j].contains(ChoisirPoint.getPoint1()))
            {

                x = (int) PosAleatoire.posx()*2;
                y = (int) PosAleatoire.posy()*2;
                xuniq = x;
                yuniq = y;
                g.fillOval((int) x, (int) y, 30, 30);
                g.setColor(Color.BLACK);
                String[] words = test[i][j].split("[>]");
                g.drawString(words[0], (int) x, (int) y);
                j++;
                while (!test[i][j-1].endsWith("|"))
                {
                    if (test[i][j].startsWith("A")) {
                        auto++;
                        if (test[i][j].contains("V.")) {
                            g.setColor(Color.BLACK);
                            ville++;
                        }
                        else if (test[i][j].contains("L.")) {
                            g.setColor(Color.RED);
                            loisir++;
                        }
                        else if (test[i][j].contains("R.")) {
                            g.setColor(Color.GREEN);
                            resto++;
                        }
                        x = (int) PosAleatoire.posx()*2;
                        y = (int) PosAleatoire.posy()*2;
                        g.fillOval( x,  y, 30, 30);
                        g.setColor(Color.BLACK);
                        g.drawLine(x+15, y+15, xuniq+15, yuniq+15);
                        words = test[i][j].split("[>]");
                        g.drawString(words[0], (x+xuniq)/2, (y+yuniq)/2);
                        g.setColor(Color.BLACK);
                        g.drawString(words[1], x, y);
                        System.out.print(test[i][j]);
                        System.out.print(" ");
                    }
                    else if (test[i][j].startsWith("D")) {
                        depart++;
                        z = 0;
                        if (test[i][j].contains("V.")) {
                            g.setColor(Color.BLACK);
                            ville++;
                        }
                        else if (test[i][j].contains("L.")) {
                            g.setColor(Color.RED);
                            loisir++;
                        }
                        else if (test[i][j].contains("R.")) {
                            g.setColor(Color.GREEN);
                            resto++;
                        }
                        x = (int) PosAleatoire.posx()*2;
                        y = (int) PosAleatoire.posy()*2;
                        g.fillOval( x,  y, 30, 30);
                        g.setColor(Color.RED);
                        g.drawLine(x+15, y+15, xuniq+15, yuniq+15);
                        words = test[i][j].split("[>]");
                        g.drawString(words[0], (x+xuniq)/2, (y+yuniq)/2);
                        g.setColor(Color.BLACK);
                        g.drawString(words[1], x, y);
                    }
                    else if (test[i][j].startsWith("N")) {
                        natio++;
                        z = 0;
                        if (test[i][j].contains("V.")) {
                            ville++;
                            g.setColor(Color.BLACK);
                        }
                        else if (test[i][j].contains("L.")) {
                            loisir++;
                            g.setColor(Color.RED);
                        }
                        else if (test[i][j].contains("R.")) {
                            resto++;
                            g.setColor(Color.GREEN);
                        }
                        x = (int) PosAleatoire.posx()*2;
                        y = (int) PosAleatoire.posy()*2;
                        g.fillOval( x,  y, 30, 30);
                        g.setColor(Color.GREEN);
                        g.drawLine(x+15, y+15, xuniq+15, yuniq+15);
                        words = test[i][j].split("[>]");
                        g.drawString(words[0], (x+xuniq)/2, (y+yuniq)/2);
                        g.setColor(Color.BLACK);
                        g.drawString(words[1], x, y);
                    }
                        j++;
                    }
            }
            else if (!test[i][j].contains(ChoisirPoint.getPoint1()))
            {
                System.out.println("Ya rien");
            }
            g.setColor(Color.BLACK);
            g.drawString("Villes = " + ville , 40, 20);
            g.fillOval( 10,  15, 15, 15);
            g.setColor(Color.RED);
            g.drawString("Loisirs = " + loisir , 40, 40);
            g.fillOval( 10,  35, 15, 15);
            g.setColor(Color.GREEN);
            g.drawString("Restos = " + resto , 40, 60);
            g.fillOval( 10,  55, 15, 15);
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(0,76,200 ,76);
            g.setColor(Color.BLACK);
            g.drawString("Autoroutes = " + auto , 40, 100);
            g.drawLine(10,90,30 ,100);
            g.setColor(Color.GREEN);
            g.drawString("Nationales = " + natio , 40, 120);
            g.drawLine(10,110,30 ,120);
            g.setColor(Color.RED);
            g.drawString("Départementales = " + depart , 40, 140);
            g.drawLine(10,130,30 ,140);
            g.setColor(Color.BLACK);
            g.drawRect(0,0,200,160);
        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {

    }
}

