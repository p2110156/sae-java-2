import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Quentin Bastos
 * @Version 1.0
 */
public class GraphGestion {
    private static String[][] GraphTab = new String[50][50];

    private static List<String> lines;

    public static String[][] getGraphTab() {
        return GraphTab;
    }

    public static void setGraphTab(String[][] graphTab) {
        GraphTab = graphTab;
    }

    public static List<String> getLines() {
        return lines;
    }

    public static void setLines(List<String> lines) {
        GraphGestion.lines = lines;
    }


    /**
     * @param args
     */

    public static void main(String[] args) {
        FenetrePrincipale frame = new FenetrePrincipale();
        frame.setVisible(true);
    }

    /**
     * Read file and return his number of line (for us his number of points)
     *
     * @param csv
     * @return
     * @author LOIC
     */
    public static int nb_neux(List<String> csv) {
        int nb = 0;
        nb = csv.size();
        System.out.println("le nombre de point est : " + nb);
        return nb; //return le nombre de lignes
    }

    /**
     * Read file and collect each line
     *
     * @param path String
     * @return List of all lines in the file
     * @throws IOException File cannot be found
     */
    public static List<String> readFile(String path) throws IOException {
        String split[];
        BufferedReader reader = new BufferedReader(new FileReader(path));
        lines = reader.lines().collect(Collectors.toList());
        reader.close();
        return lines;
    }

    /**
     * Stock a .csv file
     *
     * @return void
     * @catch ArrayIndexOutOfBoundsException e
     */
    public static void StockerFichier() {
        //se charge de catch une exception lorsque le fichier ne possède pas un bon format interne
        try{
            int i = 0;
            int j = 0;
            boolean bool;
            GraphTab[i][j] = " ";
            String test = " ";
            for (int f = 0; f < lines.size();f++) {
                test = lines.get(f);
                String[] words = test.split("[;:]");
                for (i = 0; i < words.length; i++)
                {
                    //stock le fichier dans un tableau de string
                    GraphTab[f][i] = words[i];
                    System.out.print(GraphTab[f][i]);
                }
                System.out.println(" ");
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Veillez selectionner un fichier correcte.");
            JOptionPane.showMessageDialog(null, "Type de fichier incorrect", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Open a file in memory and checking his integrity
     *
     * @return String : name of file
     * @catch exception e : wrong extension, NullPointerException e : file didn't load
     */
    public static String OuvrirFichier() {
        int bool = 1;
        String nomFic = new String();
        //se charge de catch une exception lorsque aucun fichier n'est sélectionné
        try {
            FileDialog fd = new FileDialog(new Frame(), "Sélectionnez votre fichier...", FileDialog.LOAD);
            fd.setFile("*.csv");
            fd.setVisible(true);
            nomFic = ((fd.getDirectory()).concat(fd.getFile()));
            bool = JOptionPane.showConfirmDialog(null, "Voulez-vous charger ce graph ?", "Choisir une option", JOptionPane.YES_NO_CANCEL_OPTION);
            if (!nomFic.endsWith(".csv")) {
                //se charge de catch une exception losque le fichier selectinné ne possède pas l'extention.csv
                try {
                    throw new Exception("Mon reuf faut charger des .csv stp");
                }catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Mauvaise extention de fichier !", "Erreur", JOptionPane.ERROR_MESSAGE);
                    return "";
                }
            } else if (bool == 0) {
                return nomFic;
            } else if (bool == 1) {
                nomFic = "NoFichier";
                return nomFic;
            } else if (bool == 2){
                nomFic = "Cancel";
                return nomFic;
            } else {
                return nomFic;
            }
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Fichier non chargé", "Erreur", JOptionPane.ERROR_MESSAGE);
            nomFic = "ERROR";
            return nomFic;
        }
    }
}
