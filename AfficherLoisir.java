import javax.swing.*;
import java.awt.*;

public class AfficherLoisir extends JFrame {

    public void graph() {
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setContentPane(new AfficherLoisir.MyPanel());
        setTitle("Affichage du graphe avec uniquement les centre de loisir");
        Image icon = Toolkit.getDefaultToolkit().getImage("Iko.png");
        setIconImage(icon);
        setLayout(null);
        setSize(1920, 1080);
        setVisible(true);
    }

    public AfficherLoisir() {
        super();
        graph();
        setVisible(true);
    }

    static class MyPanel extends JPanel {
        public MyPanel() {
            super();
            setPreferredSize(new Dimension(1300, 1300));
            setBorder(BorderFactory.createLineBorder(Color.black));
            // Ajouter les éléments au menu "affichage"
        }

        public void paint(Graphics p) {
            // Draw Tree Here
            int x = 160;
            double y = 160;
            int xsuiv = 210;
            int ysuiv = 260;
            int j = 0;
            int loisir = 0; // nb de loisir



            String test[][];
            test = GraphGestion.getGraphTab();
            System.out.println(test);
            p.setColor(Color.BLUE);
            int nb = GraphGestion.nb_neux(GraphGestion.getLines());
            for (int i = 0; i < nb; ) {
                if (x < 2200) {
                    if (test[i][j].startsWith("L")) {
                        loisir++;
                        System.out.println(" ");
                        System.out.print(test[i][j]);
                        p.setColor(Color.RED);
                        p.fillOval(x, (int) y, 30, 30);
                        p.setColor(Color.BLACK);
                        p.drawString(test[i][j], x, (int) y);
                        x += PosAleatoire.posx()*2+700;
                        y = PosAleatoire.posy()*2;
                    }
                }
                x = (int) PosAleatoire.posx()*2;
                i++;
            }
            p.setColor(Color.GREEN);
            p.drawString("Loisir = " + loisir , 40, 20);
            p.fillOval( 10,  15, 15, 15);
            p.setColor(Color.BLACK);
            p.drawRect(0,0,200,50);
        }
    }
}
