
public class PosAleatoire {
    private static double zonex;
    private static double zoney;
    private double max;
    private double min;

    public PosAleatoire(double zonex, double zoney, double max, double min) {
        this.zonex = zonex;
        this.zoney = zoney;
        this.max = max;
        this.min = min;
    }

    public double getZonex() {
        return zonex;
    }

    public void setZonex(double zonex) {
        this.zonex = zonex;
    }

    public double getZoney() {
        return zoney;
    }

    public void setZoney(double zoney) {
        this.zoney = zoney;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }
    public static double posx() {
        int range = (300 - 150) + 1;
        zonex = (Math.random()*range) +  150;
        return zonex;
    }
    public static double posy() {
        int range = (300 - 150) + 1;
        zoney = (Math.random()*range) +  150;
        return zoney;
    }
}
