import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.*;

/**
 *
 */
public class AfficherGraph0voisin extends JFrame implements ActionListener {

    private JMenuItem  u_Ville, u_Loisir, u_Resto, e12, e13, e14, e15, e16, e17, e18, reloadCSV;

    /**
     * Definition de la fen, taille titre, logo
     */
    public void graph() {
        setSize(1920, 1080);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setContentPane(new MyPanel());
        setTitle("Affichage du graphe");
        Image icon = Toolkit.getDefaultToolkit().getImage("Iko.png");
        setIconImage(icon);
        setLayout(null);
        setSize(1920,1080);
        setVisible(true);

        u_Ville = new JMenuItem("- Les villes");
        u_Ville.addActionListener(this);
        u_Loisir = new JMenuItem("- Les centres de loisirs");
        u_Loisir.addActionListener(this);
        u_Resto = new JMenuItem("- Les restorants");
        u_Resto.addActionListener(this);
        e12 = new JMenu("Lieux-dits");
        e13 = new JMenuItem("Afficher uniquement :");
        e14 = new JMenu("Routes");
        e15 = new JMenuItem("- Les autoroutes");
        e15.addActionListener(this);
        e16 = new JMenuItem("- Les nationnales");
        e16.addActionListener(this);
        e17 = new JMenuItem("- Les départementales");
        e17.addActionListener(this);
        e18 = new JMenuItem("Afficher uniquement :");
        reloadCSV = new JMenuItem("Charger un nouveau CSV");
        reloadCSV.addActionListener(this);

        JMenu amenu, m_reloadCSV;

        JMenuBar menubar = new JMenuBar();
        amenu = new JMenu("Affichage");
        m_reloadCSV = new JMenu("CSV");

        // Ajouter les éléments au menu "affichage"
        amenu.add(e12);
        amenu.add(e14);

        // Ajouter les éléments au menu "CSV"
        m_reloadCSV.add(reloadCSV);


        // Ajouter les éléments au sous menu "lieux-dits"
        e12.add(e13);
        e12.add(u_Ville);
        e12.add(u_Loisir);
        e12.add(u_Resto);

        // Ajouter les éléments au sous menu "Routes"
        e14.add(e18);
        e14.add(e15);
        e14.add(e16);
        e14.add(e17);

        // Ajouter le menu au barre de menu
        menubar.add(amenu);
        menubar.add(m_reloadCSV);

        this.setJMenuBar(menubar);
    }


    public AfficherGraph0voisin() {
        super();
        graph();
        setVisible(true);
    }

    static class MyPanel extends JPanel {
        public MyPanel() {
            super();
            setPreferredSize(new Dimension(300,300));
            setBorder(BorderFactory.createLineBorder(Color.black));
            // Ajouter les éléments au menu "affichage"
        }


        /**
         * @param g
         * Partie graph de la fen
         */
        @Override
        public void paint(Graphics g) {
            // Draw Tree Here
            int x = 160;
            double y = 160;
            int xsuiv = 210;
            int ysuiv = 260;
            int j = 0;
            int resto = 0; // nb de resto
            int loisir = 0; // nb de loisir
            int ville = 0; // nb de ville
            int auto = 0; // nb d'autoroute
            int depart = 0; // nb de départementale
            int natio = 0; // nb de nationale

            String test[][];
            test = GraphGestion.getGraphTab();
            System.out.println(test);
            g.setColor(Color.BLUE);
            int nb = GraphGestion.nb_neux(GraphGestion.getLines());
            for (int i = 0; i < nb;) {
                j=0;
                if (x < 1500) {
                    if (test[i][j].startsWith("V")) {
                        ville++;
                        System.out.println(" ");
                        System.out.print(test[i][j]);
                        g.setColor(Color.BLACK);
                        g.fillOval(x, (int) y, 30, 30);
                        g.setColor(Color.BLACK);
                        g.drawString(test[i][j], x, (int) y);
                        x += PosAleatoire.posx();
                        y = PosAleatoire.posy();
                        j=1;
                        while (!test[i][j-1].endsWith("|"))
                        {
                            if (test[i][j].startsWith("A")) {
                                auto++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else if (test[i][j].startsWith("D")) {
                                depart++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else if (test[i][j].startsWith("N")) {
                                natio++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else {
                                try {
                                    throw new Exception("Probleme de point dans le CSV (D, A, N)");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    JOptionPane.showMessageDialog(null, "erreur 163", "Erreur",JOptionPane.ERROR_MESSAGE);
                                    g.dispose();
                                    System.out.println("erreur 163");
                                }
                                j++;
                            }
                        }
                        i++;


                    } else if (test[i][j].startsWith("L")) {
                        loisir++;
                        System.out.println(" ");
                        System.out.print(test[i][j]);
                        g.setColor(Color.RED);
                        g.fillOval(x, (int) y, 20, 20);
                        g.setColor(Color.BLACK);
                        g.drawString(test[i][j], x, (int) y);
                        x += PosAleatoire.posx();
                        y += PosAleatoire.posy();
                        j++;
                        while (!test[i][j-1].endsWith("|"))
                        {
                            if (test[i][j].startsWith("A")) {
                                auto++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else if (test[i][j].startsWith("D")) {
                                depart++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else if (test[i][j].startsWith("N")) {
                                natio++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else {
                                try {
                                    throw new Exception("Probleme de point dans le CSV (D, A, N)");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    JOptionPane.showMessageDialog(null, "erreur 208", "Erreur critique",JOptionPane.ERROR_MESSAGE);
                                    g.dispose();
                                    System.out.println("erreur 208");
                                }
                                j++;
                            }
                        }
                        i++;
                    } else if (test[i][j].startsWith("R")) {
                        resto++;
                        System.out.println(" ");
                        System.out.println(test[i][j]);
                        g.setColor(Color.GREEN);
                        g.fillOval(x, (int) y, 20, 20);
                        g.setColor(Color.BLACK);
                        g.drawString(test[i][j], x, (int) y);
                        x += PosAleatoire.posx();
                        y += PosAleatoire.posy();
                        j++;
                        while (!test[i][j-1].endsWith("|"))
                        {
                            if (test[i][j].startsWith("A")) {
                                auto++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else if (test[i][j].startsWith("D")) {
                                depart++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else if (test[i][j].startsWith("N")) {
                                natio++;
                                j++;
                                System.out.print(test[i][j]);
                                System.out.print(" ");
                            }
                            else {
                                try {
                                    throw new Exception("Probleme de point dans le CSV (D, A, N)");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    JOptionPane.showMessageDialog(null, "erreur 251", "Erreur critique",JOptionPane.ERROR_MESSAGE);
                                    g.dispose();
                                    System.out.println("erreur 251");
                                }
                                j++;
                            }
                        }
                        i++;
                    } else {
                        try {
                            throw new Exception("Fichier Incompatible");
                        } catch (Exception e) {
                            i=nb+1;
                            e.printStackTrace();
                            JOptionPane.showMessageDialog(null, "Fichier Incompatible.", "Erreur critique",JOptionPane.ERROR_MESSAGE);
                            g.dispose();
                        }

                    }
                }
                else
                {
                    x = (int) PosAleatoire.posx();
                    y = (int) PosAleatoire.posy();
                }
            }
            g.setColor(Color.BLACK);
            g.drawString("Villes = " + ville , 40, 20);
            g.fillOval( 10,  15, 15, 15);
            g.setColor(Color.RED);
            g.drawString("Loisirs = " + loisir , 40, 40);
            g.fillOval( 10,  35, 15, 15);
            g.setColor(Color.GREEN);
            g.drawString("Restos = " + resto , 40, 60);
            g.fillOval( 10,  55, 15, 15);
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(0,76,200 ,76);
            g.setColor(Color.BLACK);
            g.drawString("Autoroutes = " + auto/2 , 40, 100);
            g.drawLine(10,90,30 ,100);
            g.setColor(Color.GREEN);
            g.drawString("Nationales = " + natio/2 , 40, 120);
            g.drawLine(10,110,30 ,120);
            g.setColor(Color.RED);
            g.drawString("Départementales = " + depart/2 , 40, 140);
            g.drawLine(10,130,30 ,140);
            g.setColor(Color.BLACK);
            g.drawRect(0,0,200,160);

        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == reloadCSV) {
            String path = GraphGestion.OuvrirFichier();
            System.out.println(path);
            List<String> graph = null;
            try {
                graph = GraphGestion.readFile(path);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            GraphGestion.StockerFichier();
            this.dispose();
            new AfficherGraph0voisin();
        }
        if (e.getSource() == u_Ville) {
                new AfficherVille();
        }
        if (e.getSource() == u_Loisir) {
            new AfficherLoisir();
        }
        if (e.getSource() == u_Resto) {
            new AfficherResto();
        }
    }
}

