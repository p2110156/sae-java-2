import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChoisirPoint extends JFrame implements ActionListener {

    private static JTextField text1;
    private static JTextField text2;
    private static JTextField text3;
    private JButton but1, but2;
    private JLabel label1, label2;
    private static String point1;
    private static String pointA;
    private static String pointB;

    public ChoisirPoint(String point1, String pointA, String pointB) throws HeadlessException {
        this.point1 = point1;
        this.pointA = pointA;
        this.pointB = pointB;
    }

    public static String getPoint1() {
        point1 = text1.getText();
        return point1;
    }

    public void setPoint1(String point1) {
        this.point1 = point1;
    }

    public static String getPointA() {
        pointA = text2.getText();
        return pointA;
    }

    public  void setPointA(String pointA) {
        this.pointA = pointA;
    }

    public static String getPointB() {
        pointB = text3.getText();
        return pointB;
    }

    public void setPointB(String point3) {
        this.pointB = pointB;
    }

    public ChoisirPoint() {
        super();
        setupWindow();
        build();
    }

    private void setupWindow() {
        setTitle("Opérations sur les chaines de caractères");
        setSize(1000, 500); //taille
        setLocationRelativeTo(null); //centrage
        setResizable(false); //non redimensionnable
    }

    private void build() {
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        label1 = new JLabel("Choissir un seul noeuds à manipuler");
        p.add(label1);
        label1.setAlignmentX(CENTER_ALIGNMENT);
        p.add(Box.createRigidArea(new Dimension(0, 90)));
        text1 = new JTextField();
        text1.setPreferredSize(new Dimension(400, 25));
        text1.setAlignmentX(Component.CENTER_ALIGNMENT);
        p.add(text1);
        p.add(Box.createRigidArea(new Dimension(0, 20)));
        but1 = new JButton("Séléctionner la valeur écrite");
        but1.addActionListener(this);
        p.add(but1);
        p.add(Box.createRigidArea(new Dimension(0, 100)));
        label2 = new JLabel("Choissir deux noeuds à manipuler A et B ");
        p.add(label2);
        label2.setAlignmentX(CENTER_ALIGNMENT);
        p.add(Box.createRigidArea(new Dimension(0, 20)));
        text2 = new JTextField();
        text2.setPreferredSize(new Dimension(400, 25));
        text2.setAlignmentX(Component.CENTER_ALIGNMENT);
        p.add(text2);
        p.add(Box.createRigidArea(new Dimension(0, 20)));
        text3 = new JTextField();
        text3.setPreferredSize(new Dimension(400, 25));
        text3.setAlignmentX(Component.CENTER_ALIGNMENT);
        p.add(text3);
        p.add(Box.createRigidArea(new Dimension(0, 20)));
        but2 = new JButton("Séléctionner les valeurs écrites");
        but2.addActionListener(this);
        p.add(but2);
        p.add(Box.createRigidArea(new Dimension(0, 90)));
        setContentPane(p);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == but1) {
            point1 = text1.getText();
        }
        if (e.getSource() == but2) {
            pointA = text2.getText();
            pointB = text3.getText();

        }
    }
}
