import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 */
public class Afficher2distances extends JFrame implements ActionListener {

    public Afficher2distances() {
        super();
        graph();
        setVisible(true);
    }

    /**
     * Definition de la fen, taille titre, logo
     */
    public void graph() {
        setSize(1920, 1080);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setContentPane(new MyPanel());
        setTitle("Affichage du graphe");
        Image icon = Toolkit.getDefaultToolkit().getImage("Iko.png");
        setIconImage(icon);
        setLayout(null);
        setSize(100, 100);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    static class MyPanel extends JPanel {
        public MyPanel() {
            super();
            setPreferredSize(new Dimension(300, 300));
            setBorder(BorderFactory.createLineBorder(Color.black));
            // Ajouter les éléments au menu "affichage"
        }


        /**
         * @param g Partie graph de la fen
         */
        @Override
        public void paint(Graphics g) {
            // Draw Tree Here
            String[] words;
            String[] words2;
            int x = 0; // variable lié au placement de la ville aleatoire
            int y = 0; // variable lié au placement de la ville aleatoire
            int xp1 = 0; // variable lié au placement de la ville 1
            int yp1 = 0; // variable lié au placement de la ville 1
            int xp2 = 0; // variable lié au placement de la ville 2
            int yp2 = 0; // variable lié au placement de la ville 2
            int z = 0; // variable lié à la matrice
            int j = 0; // variable lié à la matrice
            int i = 0; // variable lié à la matrice
            int n = 0; // variable lié à la matrice

            int k = 0;
            int q = 0;
            boolean w = true;
            int resto = 0; // nb de resto
            int loisir = 0; // nb de loisir
            int ville = 0; // nb de ville
            int auto = 0; // nb d'autoroute
            int depart = 0; // nb de départementale
            int natio = 0; // nb de nationale

            String test[][];
            test = GraphGestion.getGraphTab();
            while ((!test[i][j].equals(ChoisirPoint.getPointA()))) {
                i++;
            }

            while ((!test[z][j].equals(ChoisirPoint.getPointB()))) {
                z++;
            }

            if (test[i][j].contains(ChoisirPoint.getPointA()) && (test[z][j].contains(ChoisirPoint.getPointB()))) {
                xp1 = (int) PosAleatoire.posx() * 2;
                yp1 = (int) PosAleatoire.posy() * 2;

                xp2 = (int) PosAleatoire.posx() * 2;
                yp2 = (int) PosAleatoire.posy() * 2;


                if (test[i][j].contains("V.")) {
                    g.setColor(Color.BLACK);
                    ville++;
                } else if (test[i][j].contains("L.")) {
                    g.setColor(Color.RED);
                    loisir++;
                } else if (test[i][j].contains("R.")) {
                    g.setColor(Color.GREEN);
                    resto++;
                }
                g.fillOval(xp1, yp1, 30, 30);
                g.setColor(Color.BLACK);
                g.drawString(test[i][j], xp1, yp1);

                if (test[z][j].contains("V.")) {
                    g.setColor(Color.BLACK);
                    ville++;
                } else if (test[z][j].contains("L.")) {
                    g.setColor(Color.RED);
                    loisir++;
                } else if (test[z][j].contains("R.")) {
                    g.setColor(Color.GREEN);
                    resto++;
                }
                g.fillOval(xp2, yp2, 30, 30);
                g.setColor(Color.BLACK);
                g.drawString(test[z][j], xp2, yp2);
                j = 1;
                n = 1;

                while (((!test[i][n].endsWith("|")))) {
                    while ((!test[z][j].endsWith("|"))) {
                        words = test[i][n].split("[>]");
                        words2 = test[z][j].split("[>]");
                        if (words[1].equals(words2[1])) {
                            if (test[i][n].contains("V.")) {
                                g.setColor(Color.BLACK);
                                ville++;
                            } else if (test[i][n].contains("L.")) {
                                g.setColor(Color.RED);
                                loisir++;
                            } else if (test[i][n].contains("R.")) {
                                g.setColor(Color.GREEN);
                                resto++;
                            }
                            x = (int) PosAleatoire.posx() * 2;
                            y = (int) PosAleatoire.posy() * 2;
                            g.fillOval(x, y, 30, 30);
                            g.setColor(Color.BLACK);
                            if (words[0].startsWith("A")) {
                                auto++;
                                g.setColor(Color.BLACK);
                            } else if (words[0].startsWith("D")) {
                                depart++;
                                g.setColor(Color.RED);
                            } else if (words[0].startsWith("N")) {
                                natio++;
                                g.setColor(Color.GREEN);
                            }

                            g.drawString(words[0], (x + xp1) / 2, (y + yp1) / 2);
                            g.drawLine(x + 15, y + 15, xp1 + 15, yp1 + 15);

                            if (words2[0].startsWith("A")) {
                                auto++;
                                g.setColor(Color.BLACK);
                            } else if (words2[0].startsWith("D")) {
                                depart++;
                                g.setColor(Color.RED);
                            } else if (words2[0].startsWith("N")) {
                                natio++;
                                g.setColor(Color.BLACK);
                            }
                            g.drawString(words2[0], (x + xp2) / 2, (y + yp2) / 2);
                            g.drawLine(x + 15, y + 15, xp2 + 15, yp2 + 15);
                            g.setColor(Color.BLACK);
                            g.drawString(words[1], x, y);
                            j++;

                        } else {
                            j++;
                        }

                    }
                    words = test[i][n].split("[>]");
                    words2 = test[z][j].split("[>]");
                    if (words[1].equals(words2[1])) {
                        if (test[i][n].contains("V.")) {
                            g.setColor(Color.BLACK);
                            ville++;
                        } else if (test[i][n].contains("L.")) {
                            g.setColor(Color.RED);
                            loisir++;
                        } else if (test[i][n].contains("R.")) {
                            g.setColor(Color.GREEN);
                            resto++;
                        }
                        x = (int) PosAleatoire.posx() * 2;
                        y = (int) PosAleatoire.posy() * 2;
                        g.fillOval(x, y, 30, 30);
                        g.setColor(Color.BLACK);
                        if (words[0].startsWith("A")) {
                            auto++;
                            g.setColor(Color.BLACK);
                        } else if (words[0].startsWith("D")) {
                            depart++;
                            g.setColor(Color.RED);
                        } else if (words[0].startsWith("N")) {
                            natio++;
                            g.setColor(Color.GREEN);
                        }

                        g.drawString(words[0], (x + xp1) / 2, (y + yp1) / 2);
                        g.drawLine(x + 15, y + 15, xp1 + 15, yp1 + 15);

                        if (words2[0].startsWith("A")) {
                            auto++;
                            g.setColor(Color.BLACK);
                        } else if (words2[0].startsWith("D")) {
                            depart++;
                            g.setColor(Color.RED);
                        } else if (words2[0].startsWith("N")) {
                            natio++;
                            g.setColor(Color.BLACK);
                        }
                        g.drawString(words2[0], (x + xp2) / 2, (y + yp2) / 2);
                        g.drawLine(x + 15, y + 15, xp2 + 15, yp2 + 15);
                        g.setColor(Color.BLACK);
                        g.drawString(words[1], x, y);
                        j++;

                    } else {
                        j++;
                    }
                    j = 1;
                    n++;
                }
                words = test[i][n].split("[>]");
                words2 = test[z][j].split("[>]");
                if (words[1].equals(words2[1])) {
                    if (test[i][n].contains("V.")) {
                        g.setColor(Color.BLACK);
                        ville++;
                    } else if (test[i][n].contains("L.")) {
                        g.setColor(Color.RED);
                        loisir++;
                    } else if (test[i][n].contains("R.")) {
                        g.setColor(Color.GREEN);
                        resto++;
                    }
                    x = (int) PosAleatoire.posx() * 2;
                    y = (int) PosAleatoire.posy() * 2;
                    g.fillOval(x, y, 30, 30);
                    g.setColor(Color.BLACK);
                    if (words[0].startsWith("A")) {
                        auto++;
                        g.setColor(Color.BLACK);
                    } else if (words[0].startsWith("D")) {
                        depart++;
                        g.setColor(Color.RED);
                    } else if (words[0].startsWith("N")) {
                        natio++;
                        g.setColor(Color.GREEN);
                    }

                    g.drawString(words[0], (x + xp1) / 2, (y + yp1) / 2);
                    g.drawLine(x + 15, y + 15, xp1 + 15, yp1 + 15);

                    if (words2[0].startsWith("A")) {
                        auto++;
                        g.setColor(Color.BLACK);
                    } else if (words2[0].startsWith("D")) {
                        depart++;
                        g.setColor(Color.RED);
                    } else if (words2[0].startsWith("N")) {
                        natio++;
                        g.setColor(Color.BLACK);
                    }
                    g.drawString(words2[0], (x + xp2) / 2, (y + yp2) / 2);
                    g.drawLine(x + 15, y + 15, xp2 + 15, yp2 + 15);
                    g.setColor(Color.BLACK);
                    g.drawString(words[1], x, y);
                    j++;

                } else {
                    j++;
                }

                System.out.println("Le programme se fini bien");
            } else if (!test[i][j].contains(ChoisirPoint.getPointA()) && (!test[z][j].contains(ChoisirPoint.getPointB()))) {
                System.out.println("Ya rien");
            }

            g.setColor(Color.BLACK);
            g.drawString("Villes = " + ville, 40, 20);
            g.fillOval(10, 15, 15, 15);
            g.setColor(Color.RED);
            g.drawString("Loisirs = " + loisir, 40, 40);
            g.fillOval(10, 35, 15, 15);
            g.setColor(Color.GREEN);
            g.drawString("Restos = " + resto, 40, 60);
            g.fillOval(10, 55, 15, 15);
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(0, 76, 200, 76);
            g.setColor(Color.BLACK);
            g.drawString("Autoroutes = " + auto, 40, 100);
            g.drawLine(10, 90, 30, 100);
            g.setColor(Color.GREEN);
            g.drawString("Nationales = " + natio, 40, 120);
            g.drawLine(10, 110, 30, 120);
            g.setColor(Color.RED);
            g.drawString("Départementales = " + depart, 40, 140);
            g.drawLine(10, 130, 30, 140);
            g.setColor(Color.BLACK);
            g.drawRect(0, 0, 200, 160);

        }
    }
}


/* CODE SUPPR

                    x = (int) PosAleatoire.posx()*2;
                    y = (int) PosAleatoire.posy()*2;
                    g.fillOval(x, y, 30, 30);
                    g.setColor(Color.BLACK);
                    g.drawString(words[1], x, y);
                    g.drawString(words[0], (x+xp1)/2, (y+yp1)/2);
                    g.drawString(words2[0], (x+xp2)/2, (y+yp2)/2);
                    g.drawLine(x + 15, y + 15, xp1 + 15, yp1 + 15);
                    g.drawLine(x + 15, y + 15, xp2 + 15, yp2 + 15);

                    System.out.println(test [i][n] + " VOISIN " + test[z][j]);
                    j++;
 */

