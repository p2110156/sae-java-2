import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 *
 */
public class Comparaison2villes extends JFrame implements ActionListener {

    private JLabel entrer1;

    public Comparaison2villes() {
        super();
        graph();
        setVisible(true);
    }

    /**
     * Definition de la fen, taille titre, logo
     */
    public void graph() {
        setSize(1920, 1080);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setContentPane(new MyPanel());
        setTitle("Affichage du graphe");
        Image icon = Toolkit.getDefaultToolkit().getImage("Iko.png");
        setIconImage(icon);
        setLayout(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    static class MyPanel extends JPanel {
        public MyPanel() {
            super();
            setBorder(BorderFactory.createLineBorder(Color.black));
            // Ajouter les éléments au menu "affichage"
        }


        /**
         * @param g Partie graph de la fen
         */
        @Override
        public void paint(Graphics g) {
            // Draw Tree Here
            String[] words;
            String[] points = new String[20];// les points 1 distances sont stockés ici pour aller cherché les 2 distances
            int x = 0;
            int z = 0; // variable lié au nombre de voisin 2 sert à bloquer le while des voisins 2
            int y = 0;
            int j = 0; // variable lié à la matrice
            int i = 0; // variable lié à la matrice
            int n = 0;
            int m = 0; // variable lié au nombre de voisin 2 sert à bloquer le while des voisins 2
            StringBuilder pointA = new StringBuilder(ChoisirPoint.getPointA());
            StringBuilder pointB = new StringBuilder(ChoisirPoint.getPointB());
            StringBuilder pointA_s = pointA.deleteCharAt(0);
            pointA_s = pointA.deleteCharAt(0);
            StringBuilder pointB_s = pointB.deleteCharAt(0);
            pointB_s = pointB.deleteCharAt(0);
            int resto1 = 0; // nb de resto de ville 1
            int loisir1 = 0; // nb de loisir de ville 1
            int ville1 = 0; // nb de ville de ville 1
            int resto2 = 0; // nb de resto de ville 2
            int loisir2 = 0; // nb de loisir de ville 2
            int ville2 = 0; // nb de ville de ville 2
            int culturelle; // 1 = ville A et 2 = ville B et 3 = égal
            int ouverte; // 1 = ville A et 2 = ville B et 3 = égal
            int gastronomique; // 1 = ville A et 2 = ville B et 3 = égal
            String test[][];
            String tab[][] = new String[31][4];
            test = GraphGestion.getGraphTab();
            System.out.println(test);
            int nb = GraphGestion.nb_neux(GraphGestion.getLines());
            // Appeller plusieurs fois PosAleatoire permet d'activer la fonction aléatoire une première fois et d'éviter les bugs lorsqu'elle s'execute rééelement car (1ere valeur non aléatoire)
            x = (int) PosAleatoire.posx();
            x = (int) PosAleatoire.posx();
            y = (int) PosAleatoire.posy();
            y = (int) PosAleatoire.posy();
            x = (int) PosAleatoire.posx();
            y = (int) PosAleatoire.posy();
            g.setColor(Color.BLACK);
            for (i = 0; i < nb; ) {
                j = 0;
                if (x < 1500) {
                    x += PosAleatoire.posx();
                    y = (int) ((int) PosAleatoire.posy() * 1.5);
                } else {
                    x = (int) PosAleatoire.posx();
                    y += PosAleatoire.posy() * 1.3;
                }
                if (test[i][0].startsWith("V")) {
                    tab[i][0] = test[i][0];
                    tab[i][1] = String.valueOf(x);
                    tab[i][2] = String.valueOf(y);
                    tab[i][3] = "V";
                }
                if (test[i][0].startsWith("L")) {
                    tab[i][0] = test[i][0];
                    tab[i][1] = String.valueOf(x);
                    tab[i][2] = String.valueOf(y);
                    tab[i][3] = "L";
                }
                if (test[i][0].startsWith("R")) {
                    tab[i][0] = test[i][0];
                    tab[i][1] = String.valueOf(x);
                    tab[i][2] = String.valueOf(y);
                    tab[i][3] = "R";
                }
                i++;
            }
            i = 0;
            j = 1;
            n = 0;

            while ((!test[i][0].equals(ChoisirPoint.getPointA()))) {
                i++;
                System.out.println("test :" + test[i][0] + "    point1 :" + ChoisirPoint.getPointA());
            }
            if (test[i][j - 1].contains(ChoisirPoint.getPointA())) {
                while (!test[i][j].endsWith("|")) {
                    words = test[i][j].split("[>]");
                    if (!test[i][j - 1].contains(words[1])) {
                        if (test[i][j].contains("V.")) {
                            ville1++;
                            points[n] = words[1];
                            n++;
                        } else if (test[i][j].contains("L.")) {
                            loisir1++;
                            points[n] = words[1];
                            n++;
                        } else if (test[i][j].contains("R.")) {
                            resto1++;
                            points[n] = words[1];
                            n++;
                        }
                    }
                    j++;
                }
                words = test[i][j - 1].split("[>]");
                if (!test[i][j].contains(words[1])) {
                    if (test[i][j].contains("V.")) {
                        ville1++;
                        String[] words2 = test[i][j].split("[>]");
                        points[n] = words2[1];
                        n++;
                    } else if (test[i][j].contains("L.")) {
                        loisir1++;
                        String[] words2 = test[i][j].split("[>]");
                        points[n] = words2[1];
                        n++;
                    } else if (test[i][j].contains("R.")) {
                        resto1++;
                        String[] words2 = test[i][j].split("[>]");
                        points[n] = words2[1];
                        n++;
                    }
                    i++;
                }
            } else if (!test[i][j].contains(ChoisirPoint.getPointA())) {
                System.out.println("Ya rien");
            }
            n--;
            z = n;
            for (n = 0; n <= z; n++) {
                System.out.print("n :" + n + " ");
                System.out.println(points[n]);
            }
            i = 0;
            j = 1;
            n = 0;
            System.out.println("");
            System.out.println("");
            System.out.println("");

            for (n = 0; n <= z; n++) {
                i = 0;
                System.out.println("test :" + test[i][0] + "    point:" + points[n] + "   n :" + n);
                while ((!points[n].contains(test[i][0]))) {
                    i++;
                    System.out.println("test :" + test[i][0] + "    point:" + points[n] + "   n :" + n);
                }
                if (test[i][1].contains("|")) {

                }
                else {
                    if (points[n].contains(test[i][0])) {
                        System.out.println(test[i][0]);
                        System.out.println(test[i][1]);
                        System.out.println(test[i][2]);
                        System.out.println(j);
                        while (!test[i][j].endsWith("|")) {
                            System.out.println("test [i][j]" + test[i][j]);
                            System.out.println("while");
                            words = test[i][j].split("[>]");
                            if (!test[i][j - 1].contains(words[1])) {
                                System.out.println("contains");
                                if (test[i][j].contains("V.")) {
                                    ville1++;
                                } else if (test[i][j].contains("L.")) {
                                    loisir1++;
                                } else if (test[i][j].contains("R.")) {
                                    resto1++;
                                }
                            }
                            j++;
                        }
                        words = test[i][j - 1].split("[>]");
                        if ((!test[i][j].contains(words[1]))) {
                            if (test[i][j].contains("V.")) {
                                ville1++;
                            } else if (test[i][j].contains("L.")) {
                                loisir1++;
                            } else if (test[i][j].contains("R.")) {
                                resto1++;
                            }
                            i++;
                        }
                        j = 1;
                    } else if (!test[i][j].contains(ChoisirPoint.getPointA())) {
                        System.out.println("Ya rien");
                    }
                }
            }


            i = 0;
            j = 1;
            n = 0;

            while ((!test[i][0].equals(ChoisirPoint.getPointB()))) {
                i++;
                System.out.println("test :" + test[i][0] + "    point1 :" + ChoisirPoint.getPointB());
            }
            if (test[i][j - 1].contains(ChoisirPoint.getPointB())) {
                while (!test[i][j].endsWith("|")) {
                    words = test[i][j].split("[>]");
                    if (!test[i][j - 1].contains(words[1])) {
                        if (test[i][j].contains("V.")) {
                            ville2++;
                            points[n] = words[1];
                            n++;
                        } else if (test[i][j].contains("L.")) {
                            loisir2++;
                            points[n] = words[1];
                            n++;
                        } else if (test[i][j].contains("R.")) {
                            resto2++;
                            points[n] = words[1];
                            n++;
                        }
                    }
                    j++;
                }
                words = test[i][j - 1].split("[>]");
                if (!test[i][j].contains(words[1])) {
                    if (test[i][j].contains("V.")) {
                        ville2++;
                        String[] words2 = test[i][j].split("[>]");
                        points[n] = words2[1];
                        n++;
                    } else if (test[i][j].contains("L.")) {
                        loisir2++;
                        String[] words2 = test[i][j].split("[>]");
                        points[n] = words2[1];
                        n++;
                    } else if (test[i][j].contains("R.")) {
                        resto2++;
                        String[] words2 = test[i][j].split("[>]");
                        points[n] = words2[1];
                        n++;
                    }
                    i++;
                }
            } else if (!test[i][j].contains(ChoisirPoint.getPointA())) {
                System.out.println("Ya rien");
            }
            n--;
            z = n;
            for (n = 0; n <= z; n++) {
                System.out.print("n :" + n + " ");
                System.out.println(points[n]);
            }
            i = 0;
            j = 1;
            n = 0;
            System.out.println("");
            System.out.println("");
            System.out.println("");

            for (n = 0; n <= z; n++) {
                i = 0;
                System.out.println("test :" + test[i][0] + "    point:" + points[n] + "   n :" + n);
                while ((!points[n].contains(test[i][0]))) {
                    i++;
                    System.out.println("test :" + test[i][0] + "    point:" + points[n] + "   n :" + n);
                }
                if (test[i][1].contains("|")) {

                }
                else {
                    if (points[n].contains(test[i][0])) {
                        System.out.println(test[i][0]);
                        System.out.println(test[i][1]);
                        System.out.println(test[i][2]);
                        System.out.println(j);
                        while (!test[i][j].endsWith("|")) {
                            System.out.println("test [i][j]" + test[i][j]);
                            System.out.println("while");
                            words = test[i][j].split("[>]");
                            if (!test[i][j - 1].contains(words[1])) {
                                System.out.println("contains");
                                if (test[i][j].contains("V.")) {
                                    ville2++;
                                } else if (test[i][j].contains("L.")) {
                                    loisir2++;
                                } else if (test[i][j].contains("R.")) {
                                    resto2++;
                                }
                            }
                            j++;
                        }
                        words = test[i][j - 1].split("[>]");
                        if ((!test[i][j].contains(words[1]))) {
                            if (test[i][j].contains("V.")) {
                                ville2++;
                            } else if (test[i][j].contains("L.")) {
                                loisir2++;
                            } else if (test[i][j].contains("R.")) {
                                resto2++;
                            }
                            i++;
                        }
                        j = 1;
                    } else if (!test[i][j].contains(ChoisirPoint.getPointA())) {
                        System.out.println("Ya rien");
                    }
                }
            }
            g.setFont(new Font("20", Font.PLAIN, 20));
            if (ville1 > ville2)
            {
                System.out.println("Ville 1 PLUS OUVERTE");
                g.drawString("- " + String.valueOf(pointA_s) + " est plus ouverte que " + String.valueOf(pointB_s)+" ("+ville1+"/"+ville2+")", 660,420);

            }
            else if (ville1 == ville2)
            {
                System.out.println("Ville 1 EGAL à Ville 2");
                g.drawString("- " + String.valueOf(pointA_s) + " est autant ouverte que " + String.valueOf(pointB_s)+" ("+ville1+"/"+ville2+")", 660,420);
            }
            else {
                System.out.println("Ville 1 MOINS OUVERTE");
                g.drawString("- " + String.valueOf(pointA_s) + " est moins ouverte que " + String.valueOf(pointB_s)+" ("+ville1+"/"+ville2+")", 660,420);
            }
            if (loisir1 > loisir2)
            {
                System.out.println("Ville 1 PLUS CULTRURELLE");
                g.drawString("- " + String.valueOf(pointA_s) + " est plus culturelle que " + String.valueOf(pointB_s)+" ("+loisir1+"/"+loisir2+")", 660,380);
            }
            else if (loisir1 == loisir2)
            {
                System.out.println("Ville 1 EGAL à Ville 2");
                g.drawString("- " + String.valueOf(pointA_s) + " est autant culturelle que " + String.valueOf(pointB_s)+" ("+loisir1+"/"+loisir2+")", 660,380);
            }
            else {
                System.out.println("Ville 1 MOINS CULTRURELLE");
                g.drawString("- " + String.valueOf(pointA_s) + " est moins culturelle que " + String.valueOf(pointB_s)+" ("+loisir1+"/"+loisir2+")", 660,380);
            }
            if (resto1 > resto2)
            {
                System.out.println("Ville 1 PLUS GASTRONOMIQUE");
                g.drawString("- " + String.valueOf(pointA_s) + " est plus gastronomique que " + String.valueOf(pointB_s)+" ("+resto1+"/"+resto2+")", 660,340);
            }
            else if (resto1 == resto2)
            {
                System.out.println("Ville 1 EGAL à Ville 2");
                g.drawString("- " + String.valueOf(pointA_s) + " est autant gastronomique que " + String.valueOf(pointB_s)+" ("+resto1+"/"+resto2+")", 660,340);
            }
            else {
                System.out.println("Ville 1 MOINS GASTRONOMIQUE");
                g.drawString("- " + String.valueOf(pointA_s) + " est moins gastronomique que " + String.valueOf(pointB_s)+" ("+resto1+"/"+resto2+")", 660,340);
            }
            g.setFont(new Font("12", Font.PLAIN, 12));

            g.setColor(Color.BLACK);
            g.drawString("Villes A = " + ville1, 40, 20);
            g.fillOval(10, 15, 15, 15);
            g.setColor(Color.RED);
            g.drawString("Loisirs A = " + loisir1, 40, 40);
            g.fillOval(10, 35, 15, 15);
            g.setColor(Color.GREEN);
            g.drawString("Restos A = " + resto1, 40, 60);
            g.fillOval(10, 55, 15, 15);
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(0, 76, 200, 76);
            g.setColor(Color.BLACK);
            g.drawString("Villes B = " + ville2, 40, 100);
            g.fillOval(10, 90, 15, 15);
            g.setColor(Color.RED);
            g.drawString("Loisirs B = " + loisir2, 40, 120);
            g.fillOval(10, 110, 15, 15);
            g.setColor(Color.GREEN);
            g.drawString("Restos B = " + resto2, 40, 140);
            g.fillOval(10, 130, 15, 15);
            g.setColor(Color.BLACK);
            g.drawRect(0, 0, 200, 160);
            g.drawRect(650,320,600,338);


        }
    }
}