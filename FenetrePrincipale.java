import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

/**
 * @param /
 * @return /
 * @throws /
 */
public class FenetrePrincipale extends JFrame implements ActionListener {
    private JLabel label1, label2;
    private JButton enter1, enter2, enter3, entrer4, entrer5, entrer7;
    private JMenu menu, smenu, zmenu;
    private JMenuItem e1, e2, e3, e4, e5, e6, e7;

    public FenetrePrincipale() {
        super();
        Image icon = Toolkit.getDefaultToolkit().getImage("Iko.png");
        setIconImage(icon);
        setLayout(null);
        setSize(100,100);
        setVisible(true);
        build();
    }


    public void build() {
        // Crée un panneau
        JPanel contenuFenêtre = new JPanel();
        label1 = new JLabel("Bienvenue dans le menu qui gère le graph ! ");
        label1.setAlignmentX(Component.CENTER_ALIGNMENT);
        label2 = new JLabel("Créer par Bastos Quentin et pas Woss Loïc");
        label2.setAlignmentX(Component.CENTER_ALIGNMENT);
        entrer7 = new JButton("Afficher le graph-MAP");
        entrer7.setAlignmentX(Component.CENTER_ALIGNMENT);
        entrer7.addActionListener(this);
        enter1 = new JButton("Afficher le graph-MAP avec 0 distances");
        enter1.setAlignmentX(Component.CENTER_ALIGNMENT);
        enter1.addActionListener(this);
        enter2 = new JButton("Afficher le graph-MAP avec 1 distances");
        enter2.setAlignmentX(Component.CENTER_ALIGNMENT);
        enter2.addActionListener(this);
        entrer4 = new JButton("Savoir si 2 points sont à 2 distances");
        entrer4.setAlignmentX(Component.CENTER_ALIGNMENT);
        entrer4.addActionListener(this);
        entrer5 = new JButton("Comparer 2 villes");
        entrer5.setAlignmentX(Component.CENTER_ALIGNMENT);
        entrer5.addActionListener(this);
        enter3 = new JButton("Quitter le menu");
        enter3.setAlignmentX(Component.CENTER_ALIGNMENT);
        enter3.addActionListener(this);


        // Créer la barre de menu
        JMenuBar menubar = new JMenuBar();
        // Créer le menu
        menu = new JMenu("Fichier");
        smenu = new JMenu("CSV");
        zmenu = new JMenu("Manipulation graph");

        // Créer les éléments du menu et sous menu
        e1 = new JMenuItem("Informations complémentaires");
        e1.addActionListener(this);
        e2 = new JMenuItem(" ... ");
        e2.addActionListener(this);
        e3 = new JMenuItem("Quitter");
        e3.addActionListener(this);
        e4 = new JMenuItem("Lire le fichier CSV");
        e4.addActionListener(this);
        e5 = new JMenuItem("Text du CSV");
        e5.addActionListener(this);
        e6 = new JMenuItem("Choissir un noeuds à manipuler");
        e6.addActionListener(this);
        e7 = new JMenuItem(" ... ");
        e7.addActionListener(this);


        // Ajouter les éléments au menu
        menu.add(e1);
        menu.add(e2);
        menu.add(e3);

        // Ajouter les éléments au menu
        smenu.add(e4);
        smenu.add(e5);

        // Ajouter les éléments au sous menu
        zmenu.add(e6);
        zmenu.add(e7);


        // Ajouter le menu au barre de menu
        menubar.add(menu);
        menubar.add(smenu);
        menubar.add(zmenu);

        // Ajouter la barre de menu au frame
        this.setJMenuBar(menubar);

        // Ajoute les contrôles au panneauf.setJMenuBar(mb);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 20)));
        contenuFenêtre.add(label1);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 15)));
        contenuFenêtre.add(label2);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 20)));
        contenuFenêtre.add(entrer7);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 15)));
        contenuFenêtre.add(enter1);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 20)));
        contenuFenêtre.add(enter2);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 20)));
        contenuFenêtre.add(entrer4);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 20)));
        contenuFenêtre.add(entrer5);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 20)));
        contenuFenêtre.add(enter3);
        contenuFenêtre.add(Box.createRigidArea(new Dimension(0, 20)));
        contenuFenêtre.setLayout(new BoxLayout(contenuFenêtre, BoxLayout.Y_AXIS));
        // Crée le cadre et y ajoute le panneau

        setContentPane(contenuFenêtre);
        setSize(700, 300);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("SAE Graph de Quentin et Loïc");

    }

    public void actionPerformed(ActionEvent e) {
        // Afficher le graph-MAP basique
        if (e.getSource() == entrer7) {
            new AfficherGraph();
        }
        // Afficher le graph-MAP avec 0 distances
        if (e.getSource() == enter1) {
            new AfficherGraph0voisin();
        }
        // Afficher le graph-MAP avec 1 distances
        if (e.getSource() == enter2) {
            new Afficher1distance();
        }
        // Afficher le graph-MAP avec 2 distances
        if (e.getSource() == entrer4) {
            try{
                if(ChoisirPoint.getPointB()!=null){
                    new Afficher2distances();
                }
            }catch(NullPointerException ex){
                System.out.println("Ouverture impossible veuillez selectionner des noeuds");
                JOptionPane.showMessageDialog(null, "Veuillez effectuer une selection de noeuds", "Noeuds manquants", JOptionPane.ERROR_MESSAGE);
            }
        }
        // Afficher le graph-MAP pour comparaison de 2 distances ou plus
        if (e.getSource() == entrer5) {
            try{
                if(ChoisirPoint.getPointB()!=null){
                    new Comparaison2villes();
                }
            }catch(NullPointerException ex){
                System.out.println("Ouverture impossible veuillez selectionner des noeuds");
                JOptionPane.showMessageDialog(null, "Veuillez effectuer une selection de noeuds", "Noeuds manquants", JOptionPane.ERROR_MESSAGE);
            }
        }
        // Fermer la fenêtre principale
        if (e.getSource() == enter3) {
            dispose();
        }
        // Informations complémentaires
        if (e.getSource() == e1) {
            Informations.information();
        }
        // Stocker le csv dans la matrice
        if (e.getSource() == e2) {
            GraphGestion.StockerFichier();
        }
        // Quitter dans le menu
        if (e.getSource() == e3) {
            dispose();
        }
        // Lire le fichier CSV
        if (e.getSource() == e4) {
            String path = GraphGestion.OuvrirFichier();
            System.out.println(path);
            List<String> graph = null;
            try {
                graph = GraphGestion.readFile(path);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            GraphGestion.StockerFichier();
        }
        // Afficher un apperçu du CSV dans une nouvelle fenêtre
        if (e.getSource() == e5) {
            new CSV();
        }
        // Permet de choissir un point à manipuler
        if (e.getSource() == e6) {
            ChoisirPoint manipulation = new ChoisirPoint();
            manipulation.setVisible(true);
            // String point1 = ChoisirPoint.getPoint1(); pour avoir le point selectionner
        }
        // En construction...
        if (e.getSource() == e7) {
            System.out.println(ChoisirPoint.getPoint1());
        }
    }
}